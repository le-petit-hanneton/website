---
title: "Cabanon à outils"
description: "Un petit cabanon bien aménagé pour les outils, c'est toujours pratique !"
kind: "page"

menu:
  main:
    parent: "projets-realises"

banner: "/images/projets-realises/cabanon-a-outils/banner.jpg"

tags:
  - "Outils"
---

En mars 2021, l'intérieur du cabanon à outils a été complétement revu. Les tablards ont été inversés et un plan de travail permet de poser ses affaires. L'entiéreté du cabanon a été réfléchi pour qu'il y soit pratique de travailler et entreposer les différents outils pour le jardin.

{{< columns >}}
  {{< img
    src="/images/projets-realises/cabanon-a-outils/interieur.jpg"
    column-classes="is-half"
    figure-classes="is-3by4 m-0"
    alt="Intérieur du cabanon"
    title="Le cabanon abrite les quelques outils que l'on possède pour entretenir le jardin ainsi que la paille pour les buttes."
  >}}
{{< /columns >}}

Vous trouverez ici la liste de tout le matériel dont nous disposons. Si vous avez besoin d'un outil en particulier que nous avons, nous pouvons sans problème vous le prêter !

{{< table "projets-realises/cabanon-a-outils/outils-a-disposition" >}}

Nous mettons aussi sur la page [Soutenir - Matériel]({{< ref "/soutenir/materiel" >}}) les outils qui pourraient nous servir et que peut-être vous seriez d'accord de nous prêter au besoin.
