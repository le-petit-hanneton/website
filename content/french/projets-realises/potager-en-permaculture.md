---
title: "Potager en permaculture"
description: "Un jardin en permaculture à découvrir ou redécouvrir..."
kind: "page"

menu:
  main:
    parent: "projets-realises"

banner: "/images/potager-en-permaculture-banner.jpg"

tags:
  - "Culture"
  - "Légumes"
  - "Potager"
  - "Butte"
  - "Permaculture"
---

_Cette page doit encore être rédigée. Si vous souhaitez donner un coup de main / avoir plus d'informations avant la rédaction de la page, n'hésitez pas à nous contacter à l'aide de la page de [Contact]({{< ref "/a-propos/contact" >}} "Contact") !_
