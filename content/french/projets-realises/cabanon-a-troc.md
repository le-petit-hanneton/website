---
title: "Cabanon à troc"
description: "Je te donne ceci, tu me donnes cela, c'est trop bien !"
kind: "page"

menu:
  main:
    parent: "projets-realises"

banner: "/images/cabanon-a-troc-banner.jpg"

tags:
  - "Ouvert 24h/24"
  - "Matériel électronique"
  - "Livres"
  - "Bandes dessinées"
  - "Vaisselle"
  - "Mobilier de bureau"
  - "Tapis"
  - "Partage"
---

Un vide grenier permanant ! Venez déposer des choses dont vous ne vous servez plus et servez-vous si des choses vous intéressent ! Mobilier de bureau, bandes dessinées, matériel électronique, vaisselle, vous pouvez presque tout y trouver !

En fonction de l'intérêt rencontré, une page pourrait être dédiée si vous recherchez un objet en particulier.

Le cabanon à troc est ouvert en permanance et en dehors des meubles et des cageots en bois, tout peut être échangés ! Prenez soin du lieu et des différents objets, c'est la seule chose qu'on demande ! :smile:
