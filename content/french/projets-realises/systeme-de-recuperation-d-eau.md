---
title: "Système de récupération d'eau"
description: "Récupérer l'eau de pluie pour le jardin grâce à un système intelligent"
kind: "page"

menu:
  main:
    parent: "projets-realises"

banner: "/images/projets-realises/systeme-de-recuperation-d-eau/banner.jpg"

tags:
  - "Eau"
  - "Pluie"
  - "Arrosage"
---

Terminés en mai 2021, quatre bidons en série permettent de récolter plus d'un mètre cube (1m³) d'eau de pluie que l'on peut stocker et utiliser lors des périodes de sécheresse ou simplement lorsqu'il n'a pas plu pendant plusieurs jours.

# Liste du matériel

Pour un **bidon "de tête"** (celui sur lequel viendra se brancher un tuyau d'arrosage):

{{< table "projets-realises/systeme-de-recuperation-d-eau/bidon-de-tete" >}}

Total: **100.75 CHF**

Pour un **bidon "intermédiaire"** (celui entre deux bidons qui va se relier à ses voisins):

{{< table "projets-realises/systeme-de-recuperation-d-eau/bidon-intermediaire" >}}

Total: **82.30 CHF**

Pour un **bidon "de fin"** (celui qui sera le dernier de la série, relié à son seul voisin):

{{< table "projets-realises/systeme-de-recuperation-d-eau/bidon-de-fin" >}}

Total: **77.90 CHF**

Pour **relier les bidons** entre eux:

{{< table "projets-realises/systeme-de-recuperation-d-eau/relier-les-bidons" >}}

# Instructions de montage

_À venir_

# Photos diverses

{{< columns >}}
  {{< img
    src="/images/projets-realises/systeme-de-recuperation-d-eau/bidons-1.jpg"
    column-classes="is-half"
    figure-classes="is-4by3 m-0"
    alt="Mathilde et Ludo mesurent les bidons"
    title="Les mesures sont effectuées sur les bidons pour y placer la sortie d'eau."
  >}}
  {{< img
    src="/images/projets-realises/systeme-de-recuperation-d-eau/bidons-2.jpg"
    column-classes="is-half"
    figure-classes="is-4by3 m-0"
    alt="Mathilde aide Ludo dans le baril à fixer la plomberie"
    title="La plomberie est installée sur les bidons afin de les relier entre eux."
  >}}
  {{< img
    src="/images/projets-realises/systeme-de-recuperation-d-eau/bidons-3.jpg"
    column-classes="is-half"
    figure-classes="is-4by3 m-0"
    alt="Un baril sur des briques"
    title="Les bidons sont modifiés puis placés sur des briques."
  >}}
  {{< img
    src="/images/projets-realises/systeme-de-recuperation-d-eau/bidons-4.jpg"
    column-classes="is-half"
    figure-classes="is-3by4 m-0"
    alt="Les bidons reliés se remplissent d'eau"
    title="L'eau récoltée est répartie entre les bidons, chacun d'une capacité de 280 litres."
  >}}
  {{< img
    src="/images/projets-realises/systeme-de-recuperation-d-eau/bidons-5.jpg"
    column-classes="is-half"
    figure-classes="is-4by3 m-0"
    alt="Ludo en train de fixer l'embout Gardena"
    title="Un embout Gardena est fixé sur le premier baril pour y mettre un tuyau d'arrosage."
  >}}
  {{< img
    src="/images/projets-realises/systeme-de-recuperation-d-eau/bidons-6.jpg"
    column-classes="is-half"
    figure-classes="is-4by3 m-0"
    alt="Matthieu et Ludo en train de tester l'arrosage avec le tuyau"
    title="Le premier test avec le tuyau d'arrosage est peu concluant, mais on s'amuse quand-même."
  >}}
{{< /columns >}}
