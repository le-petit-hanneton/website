---
title: "Cours de brassage"
description: "Venez apprendre à brasser votre propre bière"
kind: "page"

menu:
  main:
    parent: "projets-en-cours"

banner: "/images/cours-de-brassage-banner.jpg"

tags:
  - "Activités"
  - "Brasserie"
  - "Bière"
---

_Cette page doit encore être rédigée. Si vous souhaitez donner un coup de main / avoir plus d'informations avant la rédaction de la page, n'hésitez pas à nous contacter à l'aide de la page de [Contact]({{< ref "/a-propos/contact" >}} "Contact") !_
