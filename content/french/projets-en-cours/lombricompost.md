---
title: "Lombricompost"
description: "Utiliser les vers de terre pour faire du compost"
kind: "page"

menu:
  main:
    parent: "projets-en-cours"

banner: "/images/lombricompost-banner.jpg"

tags:
  - "Compost"
  - "Lombric"
  - "Vers"
---

_Cette page doit encore être rédigée. Si vous souhaitez donner un coup de main / avoir plus d'informations avant la rédaction de la page, n'hésitez pas à nous contacter à l'aide de la page de [Contact]({{< ref "/a-propos/contact" >}} "Contact") !_

Pour l'instant le lombricompost est limité à 2 bacs dans notre cuisine, les vers ont survécu à de nombreuses aventures mais se portent bien.