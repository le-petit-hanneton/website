---
title: "Cours de plantes sauvages"
description: "Venez apprendre à reconnaître les plantes sauvages et comment les consommer"
kind: "page"

menu:
  main:
    parent: "projets-en-cours"

banner: "/images/cours-de-plantes-sauvages-banner.jpg"

tags:
  - "Plantes"
  - "Activités"
---

# Les plantes sauvages

La nature regorge de ressources et les plantes sauvages sont utiles autant pour
l'alimentation que les soins médicinaux. Un savoir ancestral qui se perd

Il est très important d'être sûr de l'identification de la plante pour éviter
tout risque. Ne vous fiez pas qu'à un seul sens, utilisez-les tous :

- Vue : vérifiez les formes des feuilles, fleurs; les couleurs; s'il y a des
  'poils'; la période de l'année par rapport à la taille/avancement de la vie de
  la plante
- Toucher : parfois rugueuses, parfois lisses, lisez bien la description de la
  plante; il est aussi possible de frotter des parties de la plante sur l'avant
  bras pour vérifier si la peau devient rouge, malheureusement ce n'est pas
  toujours un indicateur fiable mais ça peut être un indice
- Ouïe : quel bruit fait la plante quand on casse la tige, froisse un feuille;
  peu d'informations sont disponibles à ce sujet
- Odorat : écrasez une feuille entre les doigts et sentir. Par exemple
  l'alliaire a une odeur d'ail, si la plante ne sent pas l'ail, vous vous êtes
  probablement trompés.
- Goût : **Attention** c'est vraiment la dernière étape, il faut absolument
  vérifier avec les autres sens avant. Goûter une très petite quantité et
  comparer le goût aux descriptions de la plante puis attendre au moins 1h. Si
  l'on est pas sûr, re-goûter une quantité un peu plus importante et attendre à
  nouveau. **Attention** certaines plantes ou parties de plantes ne sont
  comestibles que cuites.

Nous vous proposons des sorties "thématiques" dans le but de partager nos
connaissances respectives, des emplacements puis partager un repas avec la
récolte. Ces sorties ne sont pas à proprement parlé des cours bien que
l'expérience s'y apparente. Pour des vrais cours Michaël Berthoud propose sur
son site https://cueilleurs-sauvages.ch des sorties au Chalet-à-Gobet, à la
Sarraz et aux Paccots. Le site regorge de contenu utile et bien rédigé.

Les sorties sont en général ouvertes à tous les niveaux, une indication est
néanmoins appliquée pour chacune d'elle.
