---
title: "Cave à champignons"
description: "Faire pousser des champignons dans sa propre cave ? C'est parti !"
kind: "page"

menu:
  main:
    parent: "projets-en-cours"

banner: "/images/cave-a-champignons-banner.jpg"

tags:
  - "Champignon"
  - "Culture"
  - "Cave"
---

_Cette page doit encore être rédigée. Si vous souhaitez donner un coup de main / avoir plus d'informations avant la rédaction de la page, n'hésitez pas à nous contacter à l'aide de la page de [Contact]({{< ref "/a-propos/contact" >}} "Contact") !_

Le but est de cultiver des champignons dans la partie innondable de la cave, les conditions y sont adaptées car climat humide, frais et sombre.
Une idée serait de combiner avec une part de lombricompost directement au sol, à voir si c'est fonctionnel ou non.
