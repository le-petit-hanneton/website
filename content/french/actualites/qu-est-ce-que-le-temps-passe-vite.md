---
title: "Qu'est-ce que le temps passe vite..."
description: "Petite retrospective des derniers mois"
author: "Le petit hanneton"
kind: "page"
draft: false
date: 2021-12-30T16:00:00+02:00
banner: "/images/actualites/qu-est-ce-que-le-temps-passe-vite/banner.jpg"
---

Et voilà, déjà six mois se sont écoulés depuis notre dernière actualité. La fin de l'année s'approche déjà pour en entamer une nouvelle. Petite rétrospective de ce qu'il s'est passé chez nous (et cette fois-ci, en vidéo !).

{{< columns >}}
  {{< img
    src="/images/actualites/qu-est-ce-que-le-temps-passe-vite/intro-1.jpg"
    date="2021-07-18 14:31"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="L'arbre tombé, coupé par les castors"
    title="Les castors se sont bien amusés dans la réserve, mais pour cette fois, ils n'auront pas pu rapporter leur butin chez eux."
  >}}
  {{< img
    src="/images/actualites/qu-est-ce-que-le-temps-passe-vite/intro-2.jpg"
    date="2021-07-21 14:05"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Nalia qui se repose"
    title="Nalia profite souvent de dormir dans le lit de Ludo depuis lequel elle peut surveiller son royaume."
  >}}
  {{< img
    src="/images/actualites/qu-est-ce-que-le-temps-passe-vite/intro-3.jpg"
    date="2021-07-22 13:36"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Des pots en terre cuite"
    title="De nouveaux pots ont été mis en place pour la saison prochaine pour y faire pousser les salades."
  >}}
  {{< img
    src="/images/actualites/qu-est-ce-que-le-temps-passe-vite/intro-4.jpg"
    date="2021-06-28 19:25"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Matthieu, grand barman"
    title="Matthieu nous prépare régulièrement de superbes cocktails, tous plus bons les uns que les autres."
  >}}
{{< /columns >}}

# Le soleil s'en est vite allé

À peine on avait cru que le soleil était arrivé, à peine notre dernière actualité publiée, que le temps en a décidé autrement.

La pluie et la grêle se sont abbatues sur notre petite habitation, laissant derrière elles.. laissant derrière elles..

Ben pas grand chose en fait.

## La grêle

Une première vague de grêle s'est abbatue sur notre jardin et contre nos vitres, faisant une couverture de glace avec la crainte que certaines fenêtres ne tiennent pas le coup. Les grêlons de quelques centimètres ont mis à mal plusieurs plants et, impuissants depuis nos fenêtres, nous ne pouvons qu'attendre que le déluge s'arrête.

Les plants, abîmés, se reprennent petit à petit et le gros semble être derrière. Hop, seconde vague de grêle pour tuer nos derniers espoirs.

La seconde vague de grêle a terminé le travail qu'elle avait commencé : quasi toutes les fleurs de nos plants sont déchiquetées, arrachées et plusieurs feuilles sont mal en point.
Nous n'avons pas beaucoup d'espoir pour notre cornichon qui était pourtant bien parti pour produire de multiples petits avec ses belles fleurs.

{{< columns >}}
  {{< video
    src="/videos/actualites/qu-est-ce-que-le-temps-passe-vite/2021-06-21-1915.mp4"
    date="2021-06-21 19:15"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Vidéo prise par Yannis de l'orage qui arrive"
    title="Lors de la première vague, Yannis était en balade avec Nalia lorsque l'orage arrivait au loin. Ludo était à la maison et Matthieu rentrait de Bulle."
  >}}
  {{< video
    src="/videos/actualites/qu-est-ce-que-le-temps-passe-vite/2021-06-21-1917.mp4"
    date="2021-06-21 19:17"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Vidéo prise par Yannis de l'orage sous un couvert"
    title="Yannis a eu le temps de se mettre à l'abri avant le déluge."
  >}}
  {{< video
    src="/videos/actualites/qu-est-ce-que-le-temps-passe-vite/2021-06-21-1918-1.mp4"
    date="2021-06-21 19:18"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Vidéo prise par Ludo depuis sa fenêtre au début de l'orage"
    title="De sa chambre, Ludo voit le début de l'orage arriver à la maison."
  >}}
  {{< video
    src="/videos/actualites/qu-est-ce-que-le-temps-passe-vite/2021-06-21-1918-2.mp4"
    date="2021-06-21 19:18"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Vidéo prise par Ludo depuis sa fenêtre pendant l'orage"
    title="Quelques secondes plus tard, c'est de plus gros grêlons qui tombent du ciel."
  >}}
  {{< video
    src="/videos/actualites/qu-est-ce-que-le-temps-passe-vite/2021-06-21-1922-1.mp4"
    date="2021-06-21 19:22"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Vidéo prise par Yannis de la tempête sous un couvert"
    title="Yannis expérimente le gros de la tempête depuis son couvert."
  >}}
  {{< video
    src="/videos/actualites/qu-est-ce-que-le-temps-passe-vite/2021-06-21-1922-2.mp4"
    date="2021-06-21 19:22"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Vidéo prise par Matthieu depuis le train avant la tempête"
    title="Matthieu rentre de Bulle et n'est pas encore dans la tempête."
  >}}
  {{< video
    src="/videos/actualites/qu-est-ce-que-le-temps-passe-vite/2021-06-21-1926.mp4"
    date="2021-06-21 19:26"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Vidéo prise par Matthieu depuis le train dans la tempête"
    title="Matthieu est maintenant lui aussi pris dans la tempête."
  >}}
  {{< img
    src="/images/actualites/qu-est-ce-que-le-temps-passe-vite/2021-06-28-1608.jpg"
    date="2021-06-28 16:08"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Notifications des alertes de pluie"
    title="Une alarme qui a tout son sens au vu des alertes météo pour la seconde vague de pluie qui aura lieu quelques jours plus tard."
  >}}
  {{< img
    src="/images/actualites/qu-est-ce-que-le-temps-passe-vite/2021-06-28-1924.jpg"
    date="2021-06-28 19:24"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Un panneau d'information suite aux dégâts de la grêle"
    title="La grêle a fait plusieurs dégâts dans la région."
  >}}
  {{< img
    src="/images/actualites/qu-est-ce-que-le-temps-passe-vite/2021-06-28-1925.jpg"
    date="2021-06-28 19:25"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Arbre tombé suite à la grêle"
    title="La grêle a fait tomber plusieurs arbres dans la région."
  >}}
{{< /columns >}}

## Période de trêve

Enfin quelques jours de tranquillité. Le froid n'encourage pas la croissance de nos plantes qui stagnent au même stade pendant plusieurs semaines.

Le chaud revient enfin et est interrompu quelques jours plus tard par la longue période de pluie qui aura fait plusieurs fois la une des nouvelles.

## La pluie et les inondations

Les jours sans cesse de pluie ont petit à petit fait sortir le lac de son lit.

Chez nous, de grosses mesures ont été mises en place pour mitiger l'inondation : interdiction d'utiliser les sanitaires, des toilettes Toitoi sont installées dans le parking. Le lac gagne du terrain, jusque dans notre jardin. Une pompe déjà installée à la cave pour évacuer l'eau lorsqu'il pleut (ce qui arrive de temps à autre, même en temps normal) commence à tourner en boucle : l'eau du lac rentre dans la cave qui est évacuée grâce à la pompe retombe à nouveau dans la cave. Toutes les cinq minutes, notre pauvre pompe essaie tant bien que mal de faire son travail et c'est finalement l'appel des pompiers qui la sauvera. L'eau est redirigée ailleurs et épargne un peu la cave le temps que le niveau du lac descende.

{{< columns >}}
  {{< video
    src="/videos/actualites/qu-est-ce-que-le-temps-passe-vite/2021-07-14-0732.mp4"
    date="2021-07-14 07:32"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Vidéo du lac qui monte"
    title="Le lac monte..."
  >}}
  {{< video
    src="/videos/actualites/qu-est-ce-que-le-temps-passe-vite/2021-07-15-2140.mp4"
    date="2021-07-15 21:40"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Vidéo du lac qui monte"
    title="Le lac monte..."
  >}}
  {{< video
    src="/videos/actualites/qu-est-ce-que-le-temps-passe-vite/2021-07-15-2141.mp4"
    date="2021-07-15 21:41"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Vidéo du lac qui monte"
    title="Le lac monte..."
  >}}
  {{< img
    src="/images/actualites/qu-est-ce-que-le-temps-passe-vite/2021-07-18-1029.jpg"
    date="2021-07-18 10:29"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="L'eau atteint notre cave"
    title="L'eau commence à atteindre notre cave..."
  >}}
  {{< img
    src="/images/actualites/qu-est-ce-que-le-temps-passe-vite/2021-07-18-1045.jpg"
    date="2021-07-18 10:45"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="La vue depuis le bureau"
    title="Le parking est sous l'eau ainsi qu'une bonne partie du jardin."
  >}}
  {{< img
    src="/images/actualites/qu-est-ce-que-le-temps-passe-vite/2021-07-18-1056.jpg"
    date="2021-07-18 10:56"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Notre jardin"
    title="Notre jardin ressemble plus à la jungle et le sol est très humide."
  >}}
  {{< img
    src="/images/actualites/qu-est-ce-que-le-temps-passe-vite/2021-07-18-1351.jpg"
    date="2021-07-18 13:51"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="La vue depuis notre bureau"
    title="Quelques heures plus tard, l'eau a atteint les escaliers de la terrasse."
  >}}
  {{< video
    src="/videos/actualites/qu-est-ce-que-le-temps-passe-vite/2021-07-20-2217.mp4"
    date="2021-07-20 22:17"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Vidéo de l'eau qui retombe la cave"
    title="L'eau évaquée par la pompe retombe dans la cave..."
  >}}
  {{< video
    src="/videos/actualites/qu-est-ce-que-le-temps-passe-vite/2021-07-21-0745.mp4"
    date="2021-07-21 07:45"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Vidéo de l'eau devant la cave"
    title="Yannis a pu fabriquer une petite digue de sable pour empêcher l'eau de couler dans notre cave."
  >}}
  {{< video
    src="/videos/actualites/qu-est-ce-que-le-temps-passe-vite/2021-07-21-1122.mp4"
    date="2021-07-21 11:22"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Vidéo de la dérive d'eau effectuée par les pompiers"
    title="Grâce aux pompiers et au matériel autour de la maison, une dérivation d'eau a pu être fabriquée pour sauver la cave."
  >}}
{{< /columns >}}

Le camping juste à côté est, pour la plupart, sous 30 à 40 centimètres d'eau. Lors d'une balade, de nombreux vers de terre sont sauvés de la noyade et sont placés dans les buttes dans lesquels, on espère, ils y trouvent un refuge. 

Le reste du jardin est sous l'eau pendant plusieurs jours mais une fois l'inondation passée, aucun réel dégât n'est à déplorer (enfin, pas pire que les dégâts précédents quoi).

{{< columns >}}
  {{< img
    src="/images/actualites/qu-est-ce-que-le-temps-passe-vite/2021-07-18-1431.jpg"
    date="2021-07-18 14:31"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Les nombreux vers de terre"
    title="Les nombreux (et gros !) vers de terre sauvés"
  >}}
  {{< img
    src="/images/actualites/qu-est-ce-que-le-temps-passe-vite/2021-07-21-1405.jpg"
    date="2021-07-21 14:05"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Le lac au pied des marches d'escaliers"
    title="Le lac redescend enfin !"
  >}}
  {{< img
    src="/images/actualites/qu-est-ce-que-le-temps-passe-vite/2021-07-22-1336.jpg"
    date="2021-07-22 13:36"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Nalia sur le sable"
    title="Nalia profite du sable frais laissé par l'inondation."
  >}}
  {{< img
    src="/images/actualites/qu-est-ce-que-le-temps-passe-vite/2021-07-23-1028.jpg"
    date="2021-06-28 19:25"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Pioche montrant le niveau d'eau dans l'espace qui deviendra l'étang"
    title="L'inodation nous aura donné l'idée de faire le futur étang là où il y avait le gravier."
  >}}
  {{< img
    src="/images/actualites/qu-est-ce-que-le-temps-passe-vite/2021-07-23-2010.jpg"
    date="2021-07-23 20:10"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Lavabo de la cave plein de sable"
    title="Le sable est remonté jusque dans le lavabo de la cave."
  >}}
  {{< video
    src="/videos/actualites/qu-est-ce-que-le-temps-passe-vite/2021-07-28-1410.mp4"
    date="2021-07-28 14:10"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Vidéo des poissons coincés suite aux inondations"
    title="Malheureusement, de petits poissons sont coincés suite aux inondations. On espère qu'ils auront pu rejoindre le lac par les rigoles."
  >}}
{{< /columns >}}

## La perte de nos "récoltes"

Mais bien que le niveau du lac redescende après plusieurs jours et que nos différentes plantations semblent survivre, ce n'était qu'une question de temps avant que toutes nos cultures piquent du nez et finissent par pourrir et mourir.. 

Il n'y a pas grand chose que nous pouvons faire et, sur les douze plants de tomates plantés en début de saison, seules trois tomates auront pu être récoltées.


{{< columns >}}
  {{< img
    src="/images/actualites/qu-est-ce-que-le-temps-passe-vite/2021-09-12-1325.jpg"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Les tomates"
    title="\"On va ptet avoir 2 tomates\" disait Yannis sur Telegram"
  >}}
  {{< img
    src="/images/actualites/qu-est-ce-que-le-temps-passe-vite/2021-09-19-1527.jpg"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="La tomate tombéé"
    title="Notre seul espoir pour les dernières tomates..."
  >}}
{{< /columns >}}


# Une fin d'été qui se termine, malgré tout, dans la positivité.

## Courges, "maïs" et patates 

Malgré tout, c'est une fin d'été qui se termine dans la positivité. Trois courges ont pu être récoltées ainsi que de nombreuses patates. Celles-ci seront transformées en frites maison et les autres pour la raclette. Elles sont excellentes. Le maïs planté fait pâle figure mais montre quelques grains sur ses différents épis tous rikikis.

Le cornichon aura quand-même réussi à produire trois immenses fruits qui s'apparentent plutôt aux concombres et termineront dans un gros bocal au vinaigre.

{{< columns >}}
  {{< img
    src="/images/actualites/qu-est-ce-que-le-temps-passe-vite/2021-07-01-1639.jpg"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="L'avant de la maison"
    title="L'avant de la maison est un peu revu pour permettre aux différentes plantes de mieux pousser à l'aide de ficelles."
  >}}
  {{< video
    src="/videos/actualites/qu-est-ce-que-le-temps-passe-vite/2021-07-02-0910.mp4"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Vidéo des buissons arrachés devant la terrasse"
    title="Les nombreux buissons piquants ont été arrachés. Ils seront remplacés par des fruits rouges comme les framboises et les raisinets."
  >}}
  {{< video
    src="/videos/actualites/qu-est-ce-que-le-temps-passe-vite/2021-07-27-1423-1.mp4"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Vidéo de l'avant de la maison avec toutes les plantes qui ont poussé"
    title="A l'avant de la maison, les plantes ont pu bien pousser."
  >}}
  {{< video
    src="/videos/actualites/qu-est-ce-que-le-temps-passe-vite/2021-07-27-1423-2.mp4"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Vidéo de l'avant de la maison avec toutes les plantes qui ont poussé"
    title="A l'avant de la maison, les plantes ont pu bien pousser."
  >}}
  {{< img
    src="/images/actualites/qu-est-ce-que-le-temps-passe-vite/2021-07-19-0725.jpg"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Les poivons issus du compost"
    title="Des poivrons issus du compost ont commencé à pousser à l'avant de la maison."
  >}}
  {{< img
    src="/images/actualites/qu-est-ce-que-le-temps-passe-vite/2021-07-19-0802.jpg"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Une salade"
    title="Une salade a survécu aux limaces !"
  >}}
  {{< img
    src="/images/actualites/qu-est-ce-que-le-temps-passe-vite/2021-07-30-2016.jpg"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Les patates"
    title="Les patates récoltées de notre jardin..."
  >}}
  {{< img
    src="/images/actualites/qu-est-ce-que-le-temps-passe-vite/2021-08-06-1959.jpg"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Une raclette"
    title="...finissent en bonne raclette."
  >}}
{{< /columns >}}

## L'étang

L'espace vidé de son gravier par les scouts de l'automne dernier subit une grande transformation grâce au travail de Yannis. Un étang est réalisé après avoir imaginé les différentes profondeurs de celui-ci et creusé un peu la terre pour se faire. Une bâche de piscine, offerte par un voisin, tapisse le fond de l'étang et c'est l'intégralité du mètre cube d'eau de pluie qui sera utilisé pour le remplir. 

Ce n'est qu'une question d'heures avant que les premières grenouilles viennent s'y installer. Dans les meilleurs jours du mois de septembre, ce ne sont pas moins de douze grenouilles qui profitent de ce nouvel espace !

Par la suite, Yannis rajoutera une petite couche de terreau permet aux différentes plantes aquatiques, scrupuleusement sélectionnées, de prospérer.

Nous nous réjouissons de voir comment cet espace sera utilisé par les différentes plantes et espérons qu'il attirera de nombreux insectes et animaux.

{{< columns >}}
  {{< img
    src="/images/actualites/qu-est-ce-que-le-temps-passe-vite/2021-09-13-1333.jpg"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="L'étang rempli d'eau"
    title="Le mètre cube d'eau rempli l'étang."
  >}}
  {{< img
    src="/images/actualites/qu-est-ce-que-le-temps-passe-vite/2021-10-23-1314-3.jpg"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="L'étang avec les plantes"
    title="Durant le mois d'octobre, Yannis a rajouté les plantes dans l'étang."
  >}}
  
{{< /columns >}}

## Supports pour les panneaux solaires

Les vacances d'automne de Ludo seront consacrées aux rideaux et au jardin. Le système de fixation des panneaux solaires est imaginé et conçu. Ceux-ci seront mis en place au printemps, permettant d'alimenter une pompe pour l'eau de pluie afin d'arroser le jardin ainsi qu'une petite fontaine pour oxygéner l'étang.

## Préparation du jardin

Même chose que l'année précédente, le jardin est préparé pour l'hiver. La haie est taillée, les feuilles mortes récoltées et un nouvel endroit pour le compost se crée derrière le cabanon à outils. Les buttes sont tondues et le compost sur les buttes pourra continuer à se décomposer durant l'hiver.

Les cinq mètres cube de bois sont commandés, livrés et stockés dans le bûcher et sur la terrasse. Un travail assez épuisant mais nécessaire pour passer l'hiver au chaud. En moyenne, un panier de bois est consommé par jour, toujours en appliquant la règle "si la température intérieure est en-dessous de 15°C, nous sommes autorisés à faire un feu" (heureusement pour nos invités et nous-mêmes, nous dérogeons parfois à cette règle). 

{{< columns >}}
  {{< img
    src="/images/actualites/qu-est-ce-que-le-temps-passe-vite/2021-10-23-1315.jpg"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Nalia dans le jardin"
    title="Nalia aide aussi à la préparation du jardin en broyant les bouts de bois."
  >}}
  {{< img
    src="/images/actualites/qu-est-ce-que-le-temps-passe-vite/2021-10-26-1235-1.jpg"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Ludo qui tond le jardin"
    title="Ludo tond le jardin pour l'hiver."
  >}}
  {{< img
    src="/images/actualites/qu-est-ce-que-le-temps-passe-vite/2021-10-26-1235-2.jpg"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Notre petite récolte"
    title="Notre petite récolte de céleri, poireaux et betteraves qui finiront en soupe."
  >}}
  {{< img
    src="/images/actualites/qu-est-ce-que-le-temps-passe-vite/2021-10-26-1323.jpg"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Le jardin tondu"
    title="Les buttes tondues, on peut les préparer pour l'hiver avec l'herbe fraîchement coupée."
  >}}
  {{< img
    src="/images/actualites/qu-est-ce-que-le-temps-passe-vite/2021-10-26-1509.jpg"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Le jardin préparé"
    title="Le jardin est préparé pour l'hiver."
  >}}
{{< /columns >}}

## Nouvelle bière de Noël

Matthieu et Yannis brassent une nouvelle bière qui sera consacrée à Noël. "Mon petit Noël" est une bière d'hiver qui veut réchauffer les cœurs et l'esprit grâce à ses doux arômes.

_Ma première bière de saison. Plongez dans l'univers des fêtes de fin d'année et enivrez vous des épices délicates qu'offre son bouquet aromatique. De quoi se réchauffer par ces températures glaciale, sa robe sombre cache bien son jeu. Joyeux Noël, restez prudent._

{{< columns >}}
  {{< img
    src="/images/actualites/qu-est-ce-que-le-temps-passe-vite/2021-08-1642-1.jpg"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Les canettes sans gaz"
    title="Les canettes, mal serties, laissaient échapper leur gaz et n'ont pas pu fermenter comme il faut."
  >}}
  {{< img
    src="/images/actualites/qu-est-ce-que-le-temps-passe-vite/2021-08-1642-2.jpg"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Les canettes refaites"
    title="Il a fallu refaire toutes les canettes avec, cette fois-ci, les bons réglages pour la machine à sertir."
  >}}
  {{< img
    src="/images/actualites/qu-est-ce-que-le-temps-passe-vite/2021-10-11-0852.jpg"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Le jardin préparé"
    title="Le fermenteur, un peu trop rempli, a \"sauté\". Heureusement, cela n'a pas affecté la qualité de la bière."
  >}}
{{< /columns >}}

## Basilic

Fait intéressant, les basilics plantés au printemps ont enfin finalement décidé de germer, pousser et grandir au meilleur moment de l'année... À la fin de l'automne. Grands de quelques centimètres, ils seront mis à l'intérieur, proche d'une fenêtre, avec pour but dessayer de les replanter au printemps. Leurs fleurs font contrastes avec l'hiver mais apportent une certaine bonne humeur dans la cuisine, en attendant les beaux jours.

## Un logo pour le petit hanneton !

Grâce au travail de Sarah, nous avons un logo qui devrait bientôt embellir le site et différentes pancartes que l'on souhaite mettre autour de la maison.

Celui-ci tente de montrer le chemin aléatoire que prend notre projet ainsi que la bonne humeur que cet insecte dégage lorsqu'il vole.

{{< columns >}}
  {{< img
    src="/images/actualites/qu-est-ce-que-le-temps-passe-vite/logo-beta.jpg"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Le logo, première version"
    title="La première version du logo !"
  >}}
{{< /columns >}}

## Les rideaux

Si la quantité des actualités a été des moindres pendant ces derniers mois, c'est bien de la faute des rideaux. On ne pouvait s'empêcher de garder le meilleur pour la fin, mais ceux-ci sont ENFIN terminés !

Depuis le temps qu'on en parle sur ce site, c'est lors des vacances d'automne de Ludo qu'une grande partie de ceux-ci seront réalisés après une optique "better done than perfect" ("mieux vaut fait que parfait") où plusieurs détails pourraient être revus mais le plus important est qu'ils soient faits. En début décembre, les derniers pans sont terminés et ce n'est pas moins de 20 pans au total qui auront été mesurés, découpés, cousus et préparés pour l'hiver.

Même si l'on maintient pas les 18°C le lendemain d'une nuit froide, la différence est notable et permet d'économiser encore un peu d'énergie pour nous chauffer.

{{< columns >}}
  {{< img
    src="/images/actualites/qu-est-ce-que-le-temps-passe-vite/2021-10-23-1314-1.jpg"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Ludo en train de faire les rideaux"
    title="Ludo, dans toute sa splendeur, en train de faire les rideaux."
  >}}
  {{< img
    src="/images/actualites/qu-est-ce-que-le-temps-passe-vite/2021-10-23-1314-2.jpg"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Ludo en train de faire les rideaux"
    title="Les rideaux doivent être mesurés, repassé (pour certains), découpés, cousus, mettre les crochets et finalement suspendus."
  >}}
  {{< img
    src="/images/actualites/qu-est-ce-que-le-temps-passe-vite/2021-10-23-1332.jpg"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Ludo en train de faire les rideaux"
    title="La nouvelle machine à coudre est bien plus efficace que l'ancienne."
  >}}
{{< /columns >}}

## Le nouvel aménagement de la cave

La cave voit son aménagement amélioré et devrait devenir plus pratique à utiliser dans le futur. Deux parties distinctes entre l'atelier et la brasserie devraient permettre aux deux activités de prospérer et ne pas trop se marcher sur les pattes des uns et des autres.

{{< columns >}}
  {{< video
    src="/videos/actualites/qu-est-ce-que-le-temps-passe-vite/2021-12-17-0802.mp4"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="La souris dans sa cage"
    title="Une souris \"ultra dodue\" a été retrouvée à la cave. Elle se baladait des fois dans le toit de la maison et a réussi à manger une partie du malt pour la bière. Elle a été libérée dans la réserve naturelle."
  >}}
  {{< img
    src="/images/actualites/qu-est-ce-que-le-temps-passe-vite/2021-12-28-1519.jpg"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="L'armoire IKEA"
    title="Une armoire IKEA, modifiée, permettra de mieux ranger les affaires situées à la cave."
  >}}
{{< /columns >}}

# Rien n'est perdu, on se retrouve l'année prochaine

Malgré les événements de cette année qui ne font que confirmer que notre environnement est en train de drastiquement changer à cause de notre façon de consommer, elle se termine à nos yeux de façon positive. La mise en place de toutes les infrastructures que nous avions en tête lors de notre emménagement ainsi que l'expérience acquise durant l'année permettront de répartir encore mieux l'année prochaine.

Dans les prochaines étapes, nous souhaitons vous proposer des actualités plus courtes mais de façon plus régulière que celles rédigées tout au long de cette année. Les panneaux solaires devraient être installés en début du printemps et le jardin sera préparé pour, on l'espère, une meilleure récolte. 

Le moral est bon, la motivation présente et c'est toujours dans la bonne humeur que le projet du petit hanneton se laisse guider pour vous accueillir et tenter de montrer qu'une autre façon de vivre est possible.

Sur ce, nous vous souhaitons une toute bonne année et à tout bientôt !

Ludo, Matthieu, Yannis et Nalia

{{< columns >}}
  {{< img
    src="/images/actualites/qu-est-ce-que-le-temps-passe-vite/nalia.jpg"
    column-classes="is-full"
    figure-classes="has-ratio m-0"
    alt="Nalia"
    title="Nalia vous souhaite une bonne année 2022 !"
  >}}
{{< /columns >}}
