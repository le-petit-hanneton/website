---
title: 'Le site est en ligne'
description: "Le début d'une grande aventure"
author: "Le petit hanneton"
kind: "page"
draft: false
date: 2020-07-19T20:08:10+02:00
banner: "/images/le-site-est-en-ligne-banner.jpg"
---

Bienvenue sur le site du petit hanneton !

Pour le moment, le contenu du site est assez pauvre et il reste encore beaucoup
de travail (autant dans le contenu que dans la forme).

Mais bon, il faut se lancer à ce qu'il paraît ! Le reste viendra, on vous le
promet.

Pour les personnes intéressées, voici quelques informations techniques concernant le site:

- Le nom de domaine est réservé chez [Infomaniak](https://www.infomaniak.com/)
- Le site est construit avec [Hugo](https://gohugo.io/)
- Les icônes sont de [Font Awesome](https://fontawesome.com/)
- Le code source est hébergé sur [GitLab](https://gitlab.com/le-petit-hanneton-x-mfp/) (les contributions sont les bienvenues !)
- Le site web est hébergé sur [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/)

Le tout nous permet pour la faramineuse somme de… 10.- par an (sans compter le temps pour le réaliser).

Voilà voilà, on espère que vous aurez du plaisir à nous suivre !

A tout bientôt,

Ludo, Matthieu et Yannis
