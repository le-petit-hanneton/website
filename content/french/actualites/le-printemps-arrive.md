---
title: "Le printemps arrive..."
description: "Et le petit hanneton sort de son hibernation"
author: "Le petit hanneton"
kind: "page"
draft: false
date: 2021-02-28T23:00:00+02:00
banner: "/images/actualites/le-printemps-arrive/banner.jpg"
---

Après quelques mois où les activités se sont plutôt faites discrètes, avec notamment le début des coutures des rideaux pour garder le froid dehors, le restockage de bois pour les nombreux feux d'hiver et le début d'une planification du jardin, le printemps arrive et, avec lui, le réveil du petit hanneton.

{{< columns >}}
  {{< img
    src="/images/actualites/le-printemps-arrive/yannis-prend-le-bois.jpg"
    column-classes="is-half"
    figure-classes="is-4by3 m-0"
    alt="Yannis en train de prendre du bois"
    title="La maison est uniquement chauffée au bois. Environ 5m³ de bois ont été nécessaires pour tenir l'hiver."
  >}}
  {{< img
    src="/images/actualites/le-printemps-arrive/le-debut-des-rideaux.jpg"
    column-classes="is-half"
    figure-classes="is-4by3 m-0"
    alt="Les rideaux dans les escaliers"
    title="La prise en main de la machine à coudre (qui mériterait une révision) n'a pas été facile, mais c'est là."
  >}}
  {{< img
    src="/images/actualites/le-printemps-arrive/le-jardin-en-hiver.jpg"
    column-classes="is-half"
    figure-classes="is-4by3 m-0"
    alt="Le jardin en hiver"
    title="Le jardin a connu quelques belles tombées de neige qui n'ont pas du tout dérangé les buttes de permacultures."
  >}}
  {{< img
    src="/images/actualites/le-printemps-arrive/un-test-de-buttes.jpg"
    column-classes="is-half"
    figure-classes="is-4by3 m-0"
    alt="Un test de buttes"
    title="Plusieurs formes de buttes ont été envisagées, avec plus ou moins de succès avec la surface et l'ensoleillement disponible."
  >}}
{{< /columns >}}

# Le mur des plantes grimpantes

L'idée de ce coin de maison est de permettre à différentes plantes grimpantes de prospérer en été (et ainsi nous faire de l'ombre dans la véranda) pour laisser place au soleil pour nous réchauffer en hiver. Une nouvelle structure remplace l'ancienne pour plus de hauteur et de stabilité.

Le palmier qui poussait contre la maison a été enlevé, ainsi que la glycine qui ornait l'entrée principale. Celle-ci devrait laisser place aux nombreux futurs haricots qui arriveront plus tard dans l'année.

{{< columns >}}
  {{< img
    src="/images/actualites/le-printemps-arrive/une-grande-glycine.jpg"
    column-classes="is-half"
    figure-classes="is-4by3 m-0"
    alt="Le jardin en hiver"
    title="Il était une fois une grande glycine au mur..."
  >}}
  {{< img
    src="/images/actualites/le-printemps-arrive/un-mur-vide.jpg"
    column-classes="is-half"
    figure-classes="is-4by3 m-0"
    alt="Le jardin en hiver"
    title="...puis, la grande glycine ne longeait plus le mur..."
  >}}
  {{< img
    src="/images/actualites/le-printemps-arrive/le-futur-mur-de-vegetation.jpg"
    column-classes="is-half"
    figure-classes="is-4by3 m-0"
    alt="Le jardin en hiver"
    title="...mais le futur mur de vegetation sera à nouveau présent et beau."
  >}}
{{< /columns >}}

# Le cabanon à outils

Une grande place autour du cabanon à outils contenait du gravier. Celui-ci a été récupéré par des scouts pour nous permettre d'y remettre de la terre (que l'on doit encore commander) et y refaire pousser de l'herbe. Cet espace devrait nous permettre de travailler autour du cabanon à l'aide d'un petit plan de travail rabatable que l'on a fabriqué et ainsi profiter du soleil quand le temps le permettra. L'intérieur du cabanon a éte réaménagé, une seconde rangée de dalles a été ajoutée pour un accès facilité et celui-ci est maintenant rempli d'outils et de matériel pour s'occuper du jardin.

{{< columns >}}
  {{< img
    src="/images/actualites/le-printemps-arrive/le-test-de-hauteur.jpg"
    column-classes="is-half"
    figure-classes="is-4by3 m-0"
    alt="Un plateau posé sur des briques"
    title="Le test de hauteur du plateau."
  >}}
  {{< img
    src="/images/actualites/le-printemps-arrive/lechec-lors-de-la-reprise-du-plateau.jpg"
    column-classes="is-half"
    figure-classes="is-3by4 m-0"
    alt="Les briques cassées"
    title="L'échec qui s'en suit."
  >}}
  {{< img
    src="/images/actualites/le-printemps-arrive/tablard-ferme.jpg"
    column-classes="is-half"
    figure-classes="is-4by3 m-0"
    alt="Le tablard fermé"
    title="Tablard fermé."
  >}}
  {{< img
    src="/images/actualites/le-printemps-arrive/tablard-ouvert.jpg"
    column-classes="is-half"
    figure-classes="is-4by3 m-0"
    alt="Le tablard ouvert"
    title="Tablard ouvert."
  >}}
  {{< img
    src="/images/actualites/le-printemps-arrive/les-deux-rangees-2.jpg"
    column-classes="is-half"
    figure-classes="is-3by4 m-0"
    alt="Les deux rangées de dalles"
    title="C'est bien plus pratique pour aller jusqu'au cabanon à outils."
  >}}
  {{< img
    src="/images/actualites/le-printemps-arrive/les-deux-rangees-1.jpg"
    column-classes="is-half"
    figure-classes="is-4by3 m-0"
    alt="Les deux rangées de dalles"
    title="Trois heures ont été nécessaires pour rajouter ces cinq dalles."
  >}}
{{< /columns >}}

# Le système de récupération d'eau de pluie

Après avoir enlevé un arbre et recouvert l'arrière du cabanon à outils avec des dalles, nous avons commencé à fabriquer notre propre système de récupération d'eau de pluie grâce à des bidons et différents éléments de plomberie. Bientôt deux bidons seront totalement terminés et plusieurs autres doivent encore être fabriqués (au moins quatre !) et reliés au reste du système.

{{< columns >}}
  {{< img
    src="/images/actualites/le-printemps-arrive/systeme-de-recuperation-deau-1.jpg"
    column-classes="is-half"
    figure-classes="is-4by3 m-0"
    alt="Un bidon sur des briques"
    title="Les bidons sont modifiés puis placés sur des briques."
  >}}
  {{< img
    src="/images/actualites/le-printemps-arrive/systeme-de-recuperation-deau-2.jpg"
    column-classes="is-half"
    figure-classes="is-3by4 m-0"
    alt="La vue d'ensemble des bidons"
    title="Quatre bidons seront mis derrière le cabanon."
  >}}
{{< /columns >}}

# Les buttes de permaculture et les semis

En novembre, des feuilles et du bois récupérés en forêt ainsi que du terreau nous ont permis de préparer deux des quatre buttes de permaculture prévues dans notre jardin ([voir le post Instagram ici](https://www.instagram.com/p/CHsGaMUn039/)). Elles semblent être bientôt prêtes à accueillir les futurs plantons que l'on a commencé à préparer ce weekend. Les 45 semis pour les futures aubergines et les poivrons / piments sont sortis dans la véranda la journée et rentrés la nuit au chaud. Dans les prochaines semaines, ces semis devraient germer (on espère) et pourront être mis en terre en temps voulu. De futurs semis sont évidemment prévus, plus de 115 variétés attendent d'être plantées (si on y arrive).

{{< columns >}}
  {{< img
    src="/images/actualites/le-printemps-arrive/semis-1.jpg"
    column-classes="is-half"
    figure-classes="is-4by3 m-0"
    alt="Des pots alignés et des graines"
    title="Les anciens yogurts sont recyclés en pot."
  >}}
  {{< img
    src="/images/actualites/le-printemps-arrive/semis-3.jpg"
    column-classes="is-half"
    figure-classes="is-4by3 m-0"
    alt="Des pots annotés"
    title="Chaque pot est scrupuleusement annoté."
  >}}
  {{< img
    src="/images/actualites/le-printemps-arrive/semis-2.jpg"
    column-classes="is-half"
    figure-classes="is-4by3 m-0"
    alt="Matthieu qui note sur les pots"
    title="Yannis met en pot, Matthieu note méticuleusement le contenu, Ludo referme les sachets."
  >}}
  {{< img
    src="/images/actualites/le-printemps-arrive/semis-4.jpg"
    column-classes="is-half"
    figure-classes="is-4by3 m-0"
    alt="Les pots alignés dans les tiroirs"
    title="Les différentes variétes de poivrons et aubergines (trois pots / variété) sont alignés dans les tiroirs d'un ancien meuble."
  >}}
  {{< img
    src="/images/actualites/le-printemps-arrive/semis-5.jpg"
    column-classes="is-half"
    figure-classes="is-4by3 m-0"
    alt="Ludo et Matthieu en train de travailler"
    title="Un travail d'équipe d'une petite heure..."
  >}}
  {{< img
    src="/images/actualites/le-printemps-arrive/semis-6.jpg"
    column-classes="is-half"
    figure-classes="is-4by3 m-0"
    alt="Selfie de Yannis"
    title="...documenté par le photographe Yannis."
  >}}
{{< /columns >}}

# Les petites bières artisanales

De nombreuses bières ont été brassées cet hiver dans la cave (plus ou moins froide). Environ trois recettes ont été testées et dégustées avec succès ! Avec les températures qui augmentent, il sera d'autant plus agréable de travailler à la cave et qui dit plus de travail à la cave (pour rester au frais), dit plus de bières brassées !

{{< columns >}}
  {{< img
    src="/images/actualites/le-printemps-arrive/matthieu-et-son-assistante.jpg"
    column-classes="is-half"
    figure-classes="is-3by4 m-0"
    alt="Matthieu et Nalia"
    title="Matthieu et son assistante Nalia."
  >}}
{{< /columns >}}

# La suite

La suite ? Quelle suite ? :wink:

Voilà une petite liste des prochains points que nous souhaitons réaliser: pose des panneaux solaires sur le toit, faire les deux dernières buttes, mettre les plantons en terre, préparer d'autres semis, enlever certains arbres/buissons, préparation du sol pour le mur de plantes grimpantes, finaliser le logo, peindre les pancartes du projet, fabriquer des nichoirs et des hôtels à insectes, mettre une ruche et plein plein d'autres choses..!

A tout bientôt,

Ludo, Matthieu, Yannis et Nalia

{{< columns >}}
  {{< img
    src="/images/actualites/le-printemps-arrive/nalia.jpg"
    column-classes="is-half"
    figure-classes="is-3by4 m-0"
    alt="Nalia"
    title="Nalia nous accompagne depuis septembre, et se retrouve, parfois, dans des situations compliquées à cause de Ludo et Matthieu, au grand désarroi de son maître Yannis."
  >}}
{{< /columns >}}
