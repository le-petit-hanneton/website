---
title: "Ressources supplémentaires"
description: "D'autres ressources qui pourraient vous intéresser !"
kind: "page"

menu:
  main:
    parent: "a-propos"

banner: "/images/ressources-supplementaires-banner.jpg"
---

# Blogs

{{< columns >}}
  {{< presentation
    column-classes="is-one-third"
    figure-classes="is-128x128 mt-6 mr-6 ml-6"
    title="La pigiste"
    img="https://blog.la-pigiste.com/wp-content/uploads/2016/11/lapigiste.jpg"
    link="https://blog.la-pigiste.com/about/"
  >}}
  Mode éthique et un cheminement vers un mode de vie plus durable,
  les consommateurs sont capables de mener un changement social important
  par le biais de leurs achats quotidiens, de leurs préférences et de leurs
  choix de vie.
  {{< /presentation >}}
{{< /columns >}}

# Revues et magazines

{{< columns >}}
  {{< presentation
    column-classes="is-one-third"
    figure-classes="is-128x128 mt-6 mr-6 ml-6"
    title="We Demain"
    img="https://www.wedemain.fr/wp-content/uploads/wedemainlogofooter.jpg"
    link="https://www.wedemain.fr/Planete_r11.html"
  >}}
  Initiatives technologiques, économiques, énergétiques, médicales, alimentaires et
  artistiques qui réinventent le monde. Publié trimestriellement au format papier.
  {{< /presentation >}}
{{< /columns >}}

# Associations et ONG

La plupart des références notées ici sont reconnues d'utilité publique et les
dons déductibles d'impôts (en Suisse)

## Locales

# Tutoriel, Do It Yourself (DIY), bricolage et autres

{{< columns >}}
  {{< presentation
    column-classes="is-one-third"
    figure-classes="is-128x128 mt-6 mr-6 ml-6"
    title="Jardi brico"
    img="https://jardi-brico.com/wp-content/uploads/2016/10/garage-ossature-bois.jpg"
    link="https://jardi-brico.com/"
  >}}
  Tutos bricolage construction, jardin, chauffage.
  {{< /presentation >}}
  {{< presentation
    column-classes="is-one-third"
    figure-classes="is-128x128 mt-6 mr-6 ml-6"
    title="ruche-naturelle.fr"
    img="https://image.jimcdn.com/app/cms/image/transf/none/path/s3c594469bf697f02/image/i6605d176080cbe69/version/1403180243/image.jpg"
    link="https://www.ruche-naturelle.fr/"
  >}}
  Tutoriel pour fabriquer des ruches naturelles. Renseignez vous concernant les législations pour l'apiculture dans votre région: [abeilles.ch](https://www.abeilles.ch/).
  {{< /presentation >}}
{{< /columns >}}

# Magasins/épiceries en vrac

{{< columns >}}
  {{< presentation
    column-classes="is-one-third"
    figure-classes="is-128x128 mt-6 mr-6 ml-6"
    title="Le chêne en vrac"
    img="https://chene-en-vrac.ch/fileadmin/templates/images/logo/logo-tc.png"
    link="https://chene-en-vrac.ch/"
  >}}
  Situé à Yvonand : Aliments principalement locaux, en partie bio;
  soins corporels et produits d'entretien.
  {{< /presentation >}}
  {{< presentation
    column-classes="is-one-third"
    figure-classes="is-128x128 mt-6 mr-6 ml-6"
    title="La vie en vrac"
    img="http://www.la-vie-en-vrac.ch/index_htm_files/3544.jpg"
    link="http://www.la-vie-en-vrac.ch/"
  >}}
  Situé à Estavayer.
  {{< /presentation >}}
  {{< presentation
    column-classes="is-one-third"
    figure-classes="is-128x128 mt-6 mr-6 ml-6"
    title="La ruche qui dit oui"
    img="/images/ressources-supplementaires-la-ruche-qui-dit-oui-logo.png"
    link="https://ruchequiditoui.ch/fr-CH"
  >}}
  Réseau de magasins (25 en Suisse romande) dont un à Cugy FR, un mini à Payerne.
  {{< /presentation >}}
{{< /columns >}}

# Vente direct à la ferme

{{< columns >}}
  {{< presentation
    column-classes="is-one-third"
    figure-classes="is-128x128 mt-6 mr-6 ml-6"
    title="Terroir Fribourg"
    img="https://bulma.io/images/placeholders/128x128.png"
    link="https://www.terroir-fribourg.ch/fr/produits/vente/vente-directe"
  >}}
  {{< /presentation >}}
  {{< presentation
    column-classes="is-one-third"
    figure-classes="is-128x128 mt-6 mr-6 ml-6"
    title="Marché paysan"
    img="https://bulma.io/images/placeholders/128x128.png"
    link="https://www.marchepaysan.ch/"
  >}}
  {{< /presentation >}}
{{< /columns >}}

# Entreprises durables

{{< columns >}}
  {{< presentation
    column-classes="is-one-third"
    figure-classes="is-128x128 mt-6 mr-6 ml-6"
    title="La maison au naturel"
    img="https://bulma.io/images/placeholders/128x128.png"
    link="https://www.maison-nat.ch/"
  >}}
  Construction et rénovation de bâtiments.
  {{< /presentation >}}
{{< /columns >}}
