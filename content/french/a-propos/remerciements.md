---
title: "Remerciements"
description: "Sans vous, ce projet ne serait pas grand chose"
kind: "page"

menu:
  main:
    parent: "a-propos"

banner: "/images/remerciements-banner.jpg"
---

Merci à toutes les personnes qui ont contribués de près ou de loin à faire de
cette grande idée une réalité.

Merci à tout ceux qui, après avoir parcouru ce site / visité notre lieu de vie /
discuté avec nous, ont entrepris des changements dans votre vie.

Merci aussi pour votre soutien, les fous-rires, vos idées, votre bonne humeur et
volonté, votre énergie, votre positivité.

- Colin
- Justine
- Sarah
- Mathilde
- Isabelle
- Miguel
- Et toutes les autres personnes qu'on aurait oublié !
