---
title: "À propos"
description: ""
kind: "page"

cascade:
  banner: "/images/a-propos-banner.jpg"
---

Le projet est né d'une volonté commune et fait notamment suite au projet "[Jette
pas !]({{< ref "/projets-realises/jette-pas" >}})" que Ludo a développé au sein
de l'incubateur Impact Hub à Lausanne. Testé avec succès dans plusieurs lieux,
ce projet consiste à mettre à disposition un lieu de rencontre pour faire
réparer des objets de tous types, à la manière d'un Repair Café. Mais ceux-ci
étant en général temporaires, l'idée serait à terme de créer des lieux
"combinés" permanents, le concept est encore mené à évoluer.

Les autres projets sont à des stades plus ou moins avancés et de nouvelles idées
vont probablement émerger. Notre but est de créer un espace communautaire avec
de multiples facettes et compétences où le partage est le mot d'ordre. Si vous
avez des projets en tête mais manquez de ressources (mains et cerveaux),
n'hésitez pas à nous en faire part, nous transmettrons à ce réseau qu'on espère
sera florissant.

L'autonomie, l'autosufficience (ou communosufficience), le respect de la nature,
la durabilité et la reconnexion entre humains se regroupent dans un objectif de
réduction de l'impact que nous avons sur la planète tout en améliorant notre
qualité de vie.
