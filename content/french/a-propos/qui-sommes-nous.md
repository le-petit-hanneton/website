---
title: "Qui sommes-nous ?"
description: "Les trois cons pairs"
kind: "page"

menu:
  main:
    parent: "a-propos"

banner: "/images/qui-sommes-nous-banner.jpg"
---

Vous voulez connaître un peu les personnes qui poursuivent ce projet ? N'hésitez pas à passer à la maison ou simplement lire les petites descriptions ci-dessous !

{{< columns >}}
  {{< presentation
    column-classes="is-one-third"
    figure-classes="is-128x128 mt-6 mr-6 ml-6"
    title="Yannis"
    img="/images/qui-sommes-nous-yannis.jpg"
  >}}
  Un peu perdu à la suite d'études d'ingénieur en informatique en cours d'emploi
  qui n'ont pas vraiment laissé de temps pour penser, je me pose toutes les
  questions maintenant. En plein voyage initiatique et exploratoire au coeur de
  moi-même, j'aspire à trouver un/des moyens d'être heureux sans être ruiné.

  Gentil mais parfois insolent, mon éloquence (ironie) n'a d'égal que mon
  imbécilité. Mon compagnon à 4 pattes (Nalia) m'aide à me reconnecter avec
  l'extérieur et la nature.
  {{< /presentation >}}

  {{< presentation
    column-classes="is-one-third"
    figure-classes="is-128x128 mt-6 mr-6 ml-6"
    title="Matthieu"
    img="/images/qui-sommes-nous-matthieu.jpg"
  >}}
  Toujours prêt à filer un coup de main, je suis d'un naturel patient.
  Curieux et avide d'apprendre de nouvelles choses, j'aime aussi transmettre mes connaissances aux personnes intéressées.
  Je vis au jour le jour en essayant de planifier le moins possible.
  N'ayant pas trouvé ma voie dans l'informatique j'explore d'autres pistes.

  Décalé avec la société actuelle, j'essaie de mettre à disposition mes compétences et mes réflexions
  J'ai plein de projets en tête et cherche une manière éthique de les réaliser.
  {{< /presentation >}}

  {{< presentation
    column-classes="is-one-third"
    figure-classes="is-128x128 mt-6 mr-6 ml-6"
    title="Ludo"
    img="/images/qui-sommes-nous-ludo.jpg"
  >}}
  Subtile mélange entre l'utopiste et le pessimiste, j'aime toucher à tout et réimaginer
  le présent. Beaucoup trop d'idées, jamais assez de temps, personnage un peu atypique,
  les conventions ne sont pas faites pour moi; je préfère vivre selon mes propres valeurs
  et convictions.

  Pour faire face à notre mode de vie actuel et aux conséquences que cela engendre sur
  la nature, j'essaie d'apporter mes propres solutions sociales et environnementales
  au travers du petit hanneton.
  {{< /presentation >}}
{{< /columns >}}
