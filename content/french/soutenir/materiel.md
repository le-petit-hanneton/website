---
title: "Matériel"
description: "Construire, réparer, réutiliser et partager !"
kind: "page"

menu:
  main:
    parent: "soutenir"

banner: "/images/materiel-banner.jpg"
---

Vous trouverez ici la liste de tout le matériel que l'on cherche à acquérir. Il est plus intéressant pour nous de trouver des personnes capables de nous prêter ces objets afin de réduire notre impact environmental ! Nous ne cherchons pas à absolument avoir ces objets 24h/24 chez nous :smile:.

Nous maintenons une liste des outils que l'on a déjà et que l'on prête volontiers sur la page [Cabanon à outils]({{< ref "/projets-realises/cabanon-a-outils" >}}). Si vous avez besoin d'un outil en particulier que nous avons, nous pouvons sans problème vous le prêter !

{{< table "soutenir/materiel" >}}
