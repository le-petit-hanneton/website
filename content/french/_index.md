---
title: "Le petit hanneton"
description: "Le projet aux allures aléatoires"
kind: "page"

cascade:
  banner: "/images/accueil-banner.jpg"

_build:
  list: never
  publishResources: false
---

Le projet du petit hanneton est issu de réflexions entre trois jeunes qui
avaient le souhait de créer un lieu où ils pourraient vivre au plus proche de
leurs valeurs.

Ces trois colocataires ont trouvé une petite maison, au bord du lac de Neuchâtel
entre Yvonand et Estavayer-le-Lac, avec un potentiel fou. Ils vous invitent à
les suivre, au travers de ce site et ailleurs, dans leur quête de créer un lieu
communautaire, écoresponsable, agréable à vivre et vous invite tou.te.s à
rejoindre l'aventure !

Le nom "Le petit hanneton" vient de cette idée que le hanneton est un insecte
qui, après être resté de nombreuses années sous terre, prend son envol sans
savoir comment réellement bien voler. Il se déplace de façon complètement
aléatoire et on ne comprend pas où il va ou ce qu'il fait.

Tout comme nous. Nous ne savons pas ce que nous faisons mais nous essayons de
réaliser ce projet aux allures aléatoires.

Sous ce nom se regroupe de nombreux projets, idées, aménagements, services de
durabilité afin de vivre une vie heureuse, harmonieuse et en accord avec nos
valeurs et principes. Notre maison est située à Cheyres et notre crédo est
l'aléatoire car c'est toujours les décisions les plus aléatoires qui font
prendre une nouvelle direction à nos vies et nous rendent heureux.

Nos ambitions, nos envies, les échanges avec l'autre et l'entraide nous
poussent à apprendre et découvrir de nouvelles façons de vivre et s'épanouir dans
notre quotidien. C'est ce que l'on souhait partager avec vous. Si vous êtes
curieu.ses.x, intéressé.e.s à nous aider ou simplement tombé.e.s ici aléatoirement, nous
espèrons que vous trouverez le contenu de ce site utile et qu'il vous inspirera à
faire de même chez vous !

Si vous passez dans la région et souhaitez découvrir notre habitation, notre
porte est toujours ouverte pour discuter de tout et de rien et même boire une
bière locale :wink:.

Bonne visite !

Ludo, Matthieu et Yannis
