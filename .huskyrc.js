const tasks = (arr) => arr.join(" && ");

module.exports = {
	hooks: {
		"pre-push": tasks([
			// check outdated packages
			// "npm run packages:check-outdated",
			// audit packages
			// "npm run packages:audit",
			// review code format
			"npm run review:code:format",
		]),
	},
};
