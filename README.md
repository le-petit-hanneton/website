# **Le petit hanneton** - Website

[![Pipeline Status][pipeline-status-badge]][pipeline-status-url]
[![Code License][code-license-badge]][code-license-url]
[![Content License][content-license-badge]][content-license-url]
[![Code of Conduct][code-of-conduct-badge]][code-of-conduct-url]
[![Contributing][contributing-badge]][contributing-url]

## Introduction

**Le petit hanneton** is a project based in Cheyres, Switzerland conducted by
three young people to create a living and vibrant community around their house:
permaculture, barter place, repair shop, beer crafting, among many others
projects. See the [official website](https://le-petit-hanneton.ch) for more
details!

## Documentation

More documentation for this repository can be found in the [`docs`](docs)
directory.

## License

The code is licensed under the **MIT License** - see the
[`LICENSE`][code-license-url] file for details.

The content is licensed under the **Creative Commons
Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0)
License** - see the [Creative Commons][content-license-url] website for details.

## Contributing

Thank you for considering contributing to Le petit hanneton! Here is some help
to get you started to contribute to the project:

1. Please start by reading our code of conduct available in the
   [`CODE_OF_CONDUCT.md`][code-of-conduct-url] file.
2. All contribution information is available in the
   [`CONTRIBUTING.md`][contributing-url] file.

Feel free to contribute to the project in any way that you can think of, your
contributions are more than welcome!

[pipeline-status-badge]:
  https://gitlab.com/le-petit-hanneton-x-mfp/le-petit-hanneton/badges/master/pipeline.svg
[pipeline-status-url]:
  https://gitlab.com/le-petit-hanneton-x-mfp/le-petit-hanneton/commits/master
[code-license-badge]: https://img.shields.io/badge/code%20license-MIT-blue.svg
[code-license-url]: LICENSE
[content-license-badge]:
  https://img.shields.io/badge/content%20license-CC%20BY--SA%204.0-lightgrey.svg
[content-license-url]: https://creativecommons.org/licenses/by-nc-sa/4.0/
[code-of-conduct-badge]:
  https://img.shields.io/badge/code%20of%20conduct%20-Contributor%20Covenant%20v2.0-ff69b4.svg
[code-of-conduct-url]: CODE_OF_CONDUCT.md
[contributing-badge]:
  https://img.shields.io/badge/contributing-GitLab%20workflow-fca326.svg
[contributing-url]: CONTRIBUTING.md
[changelog-badge]:
  https://img.shields.io/badge/changelog-Keep%20a%20Changelog%20v1.1.0-%23E05735.svg
[changelog-url]: CHANGELOG.md
