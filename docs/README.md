# **Le petit hanneton** - Website documentation

This is the documentation for the website of **Le petit hanneton**. Have a look
at the [repository](https://gitlab.com/le-petit-hanneton-x-mfp/le-petit-hanneton) or the
[official website](https://le-petit-hanneton.ch) for more details.

## Designs

These designs should help to think about the website and how to present the
information on it.

### Colors

The color palette has been done with [Coolors](https://coolors.co/).

As a primary test,
[the following color palette](https://coolors.co/e9e387-f4c190-ff9f98-d1abaf-a2b6c5-a79fa3-ab8781-857c7a-5e7173-383d3b)
has been chosen:

- `#E9E387`
- `#F4C190`
- `#FF9F98`
- `#D1ABAF`
- `#A2B6C5`
- `#A79FA3`
- `#AB8781`
- `#857C7A`
- `#5E7173`
- `#383D3B`

### Mockups

These mockups should give the idea of the final website:

- [Homepage](https://www.figma.com/proto/HoJoyd0vJFb8AWbRYs1DFI/Le-petit-hanneton?node-id=6%3A3&scaling=min-zoom) -
  The homepage of the website when accessing
  [https://le-petit-hanneton.ch](https://le-petit-hanneton.ch/)
- [Single page](https://www.figma.com/proto/HoJoyd0vJFb8AWbRYs1DFI/Le-petit-hanneton?node-id=6%3A6&scaling=min-zoom) -
  A single page of the website, containing the contact information for instance.
- [List page](https://www.figma.com/proto/HoJoyd0vJFb8AWbRYs1DFI/Le-petit-hanneton?node-id=19%3A1&scaling=min-zoom) -
  A list page of the website, containing all the blog posts for instance.
- [Post page](https://www.figma.com/proto/HoJoyd0vJFb8AWbRYs1DFI/Le-petit-hanneton?node-id=19%3A5&scaling=min-zoom) -
  A post page of the website, containing the entire blog post for instance.

### Logos

_TODO_

### Icons

_TODO_

### Images

_TODO_

## Run the project

### Prerequisites

The following prerequisites must be installed to run this project:

- [`git`](https://git-scm.com/) - A free and open source distributed version
  control system.
- [Hugo with "extended" Sass/SCSS support](https://gohugo.io/) - The world’s fastest framework for building
  websites.
- [Node.js](https://nodejs.org) - A JavaScript runtime built on Chrome's V8
  JavaScript engine.

### Clone the repository

```sh
# Clone the repository
git clone https://gitlab.com/le-petit-hanneton-x-mfp/le-petit-hanneton.git

# Move to the cloned directory
cd le-petit-hanneton

# Install the dependencies
npm install

# Set up all the submodules
git submodule sync --recursive
git submodule update --init --recursive
```

### Start the project

```sh
# Start the project
hugo server -D
```

The live website is then available on
[http://localhost:1313/](http://localhost:1313/).

## Technologies used

### Frameworks and libraries

- [Bulma](https://bulma.io/) - A free, open source CSS framework based on
  Flexbox.
- [Hugo](https://gohugo.io/) - The world’s fastest framework for building
  websites.
- [Husky](https://typicode.github.io/husky/) - Prevent bad `git commit`,
  `git push` and more.
- [Node.js](https://nodejs.org) - A JavaScript runtime built on Chrome's V8
  JavaScript engine.
- [Prettier](https://prettier.io/) - An opinionated code formatter.
- [PostCSS](https://postcss.org/) - A tool for transforming CSS with JavaScript.
- [Sass](https://sass-lang.com/) - The most mature, stable, and powerful
  professional grade CSS extension language in the world.

### Quality, performance and security audit tools

_TODO_

### Tools and plugins

- [Coolors](https://coolors.co/) - Create the perfect palette or get inspired by
  thousands of beautiful color schemes.
- [Figma](https://www.figma.com/) - Figma helps teams create, test, and ship
  better designs from start to finish.
- [Visual Studio Code](https://code.visualstudio.com/) - A code editor redefined
  and optimized for building and debugging modern web and cloud applications.
  - [EditorConfig for VS Code](https://marketplace.visualstudio.com/items?itemName=EditorConfig.EditorConfig) -
    Override user/workspace settings with settings found in `.editorconfig`
    files.
- Images optimizers _TODO_


### Images optimization

`sudo apt install imagemagick`

`npm install -g smartcrop-cli`
