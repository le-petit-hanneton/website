module.exports = {
	printWidth: 80,
	singleQuote: false,
	semi: true,
	trailingComma: "all",
	proseWrap: "always",
	overrides: [
		{
			files: ["*.html"],
			options: {
				parser: "go-template",
			},
		},
	],
};
